import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import styles from './MiddleInfo.styl';
import colors from '../../colors/colors.styl';

import Slider from "react-slick";

const infoCycle = require('../../data/infoCycle');

var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplaySpeed: 2500,
    // autoplay: true,
};

class MiddleInfo extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <div className={styles.container}>
                    <h2 className="textMiddle">Info Here</h2>
                    <div>
                        <Slider {...settings}>
                            {infoCycle.infoCycle.map((item, index) =>
                                <div key={index} className={styles.eachItemInCycleContainer}>
                                    <div className={styles.eachItemInCycle}>
                                        <h3>{item.key}</h3>
                                    </div>
                                </div>
                            )}
                        </Slider>
                    </div>
                </div>
            </React.Fragment >
        );
    }
}

export default MiddleInfo;
