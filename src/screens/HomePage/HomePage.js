import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import styles from './HomePage.styl';
import colors from '../../colors/colors.styl';
import StageTool from '../StageTool/StageTool';
import Digestif from '../Digestif/Digestif';
import MiddleInfo from '../MiddleInfo/MiddleInfo';
import IntroSection from '../IntroSection/IntroSection';

class HomePage extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <div className={styles.container}>
                    <div>
                        <IntroSection></IntroSection>
                    </div>
                    <div className={styles.eachContainer}>
                        <StageTool></StageTool>
                    </div>
                    <div className={styles.eachContainer}>
                        <MiddleInfo></MiddleInfo>
                    </div>
                    <div className={styles.eachContainer}>
                        <Digestif></Digestif>
                    </div>
                </div>
            </React.Fragment >
        );
    }
}

export default HomePage;
