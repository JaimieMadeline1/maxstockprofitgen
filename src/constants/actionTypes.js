// loading.js
const LOADING_ON = 'LOADING_ON';
const LOADING_OFF = 'LOADING_OFF';

// auth.js
const AUTH_LOGIN = 'AUTH_LOGIN';
const AUTH_LOGOUT = 'AUTH_LOGOUT';
const AUTH_PRELOG = 'AUTH_PRELOG';

// userState.js
const USER_ADD_USER = 'USER_ADD_USER';
const USER_CHANGE_USER_EMAIL = 'USER_CHANGE_USER_EMAIL';
const USER_CHANGE_USER_NAME = 'USER_CHANGE_USER_NAME';
const USER_RESET = 'USER_RESET';

// history.js
const HISTORY_CLEAR = 'HISTORY_CLEAR';
const HISTORY_ADD_SEARCH = 'HISTORY_ADD_SEARCH';

export {
    LOADING_ON,
    LOADING_OFF,

    AUTH_LOGIN,
    AUTH_LOGOUT,
    AUTH_PRELOG,

    USER_ADD_USER,
    USER_CHANGE_USER_EMAIL,
    USER_CHANGE_USER_NAME,
    USER_RESET,

    HISTORY_CLEAR,
    HISTORY_ADD_SEARCH
}
