import {
    HISTORY_CLEAR,
    HISTORY_ADD_SEARCH
} from '../constants/actionTypes';

const intialState = { pastStocks: [] };

export const history = (state = intialState, action) => {
    switch (action.type) {
        case HISTORY_CLEAR:
            return { ...state, pastStocks: [] };
            break;
        case HISTORY_ADD_SEARCH:
            return { ...state, pastStocks: [...state.pastStocks, action.searchObj] };
            break;
        default:
            return state;
    }
}