export const infoCycle = [
    { key: 'Webpack 3.11.0' },
    { key: 'Redux' },
    { key: 'Redux Pesist using LocalForage(underthe hood IndexDB, being the Async and better solution compared to localStorage)' },
    { key: 'Routing using React-Routing-Dom' },
    { key: 'Bootstrap v4 with Reactstrap' },
    { key: 'Separate Dev and Prod environment setup' },
    { key: 'redux-devtools on screen in Dev mode' },
    { key: 'Stylus CSS library with CSS modules configuration(webpack configured to also allow global styl files to work in their own folder). Press format shortcut inside a styl files for it to work properly.' },
    { key: "Configured Manta's Stylus Supremacy to format stylus files properly(default settings formats too much)" },
    { key: "Centralized system for colors in colors/colors.styl, In Stylus: @import '../../colors/colors' In Javascript: import colors from '../../colors/colors.styl';" },
    { key: 'Global styles in styles/styles.styl, although still need to be imported due to CSS Module approach' },
    { key: 'Axios library to fetch requests' },
    { key: 'Uses OpenBrowserPlugin to open page in browser upon startup' },
    { key: 'Uses File Loader to use images in assets folder' },
    { key: 'Hosted via firebase.' }
];