import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { ClipLoader } from 'react-spinners';

import homeStyles from './Home.styl';
import colors from '../../colors/colors.styl';

// Global Styles, imported only once, folder not under css modules, so can be used in any file
import globalStyles from '../../styles/globalStyles.styl';

import Global from '../Global';
import Header from '../Header/Header';
import Footer from '../Footer';

import RightMenu from '../../routing/RightMenu';


class Home extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <Global></Global>
                <Header></Header>
                <div className={homeStyles.mainStyle}>
                    {/* <div className="container"> */}
                        {/* <div className="row"> */}
                            {/* <div className="col-lg-1"></div> */}
                            {/* <div className="col-lg-12"> */}
                                <RightMenu></RightMenu>
                            {/* </div> */}
                            {/* <div className="col-lg-1"></div> */}
                        {/* </div> */}
                    {/* </div> */}
                </div>
                <Footer></Footer>
            </React.Fragment >
        );
    }
}

export default Home;
