import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import Home from '../screens/Home/Home';
// import { ApolloProvider, createNetworkInterface } from 'react-apollo';

import gql from "graphql-tag";

const appConstants = require('../constants/appConstants');

// import createHistory from 'history/createBrowserHistory';

import { PersistGate } from 'redux-persist/integration/react';

// Create a history of your choosing (we're using a browser history in this case)
// const history = createHistory();

// import ApolloClient from "apollo-boost";

// const client = new ApolloClient({
//     // uri: "https://w5xlvm3vzz.lp.gql.zone/graphql"
//     uri: "https://www.alphavantage.co/query?function=TIME_SERIES_WEEKLY&symbol=MSFT&apikey=demo"
// });

class Main extends Component {
    constructor() {
        super();
        // client.query({
        //     query: gql`
        //             {
        //             }
        //             `
        // }).then(result => console.log(result));
    }
    render() {
        return (
            <React.Fragment>
                <PersistGate loading={null} persistor={this.props.persistor}>
                    <React.Fragment>
                        {/* <ApolloProvider client={client}> */}
                            <BrowserRouter>
                                <Home></Home>
                            </BrowserRouter>
                        {/* </ApolloProvider> */}
                    </React.Fragment>
                </PersistGate>
            </React.Fragment>
        )
    }
}

export default Main;
