import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { ClipLoader } from 'react-spinners';

import styles from './History.styl';
import colors from '../../colors/colors.styl';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

import { connect } from 'react-redux';
import {
    HISTORY_CLEAR
} from '../../constants/actionTypes';

import BootstrapTable from 'react-bootstrap-table-next';

const columns = [{
    dataField: 'stockName',
    text: 'Ticker',
    sort: true
}, {
    dataField: 'time',
    text: 'Date',
    sort: true
}];


class History extends React.Component {
    constructor(props) {
        super(props);
        let newList = [];
        this.props.pastStocks.forEach((item) => {
            newList.push({
                stockName: item.stockName.toUpperCase(), time: new Date(item.time).toLocaleString()
            });
        })

        this.state = {
            pastSearchs: newList, tempPastSearchs: newList, tickerInput: ''
            // finishSelectingTickerSymbol: false, searchSuggestionSelectedIndex: 0,
            // tempSearchTickerSuggestions: [], inputTickerfocused: true
        };

        this.tempSearchTickerSuggestions = [];
    }
    formatDate(date) {
        let monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];

        let day = date.getDate();
        let monthIndex = date.getMonth();
        let year = date.getFullYear();

        return day + ' ' + monthNames[monthIndex] + ' ' + year;
    }
    clearHistory() {
        this.props.clearHistory();
        this.setState({ tempPastSearchs: [], pastSearchs: [] });
    }
    onTickerInputChange(e) {
        this.setState({ tickerInput: e.target.value });
        // tempPastSearchs
        let tempData = this.state.pastSearchs.filter((el) => {
            // this.filterHelper(el);
            return el.stockName.toLowerCase().indexOf(e.target.value.toLowerCase()) > -1;
        });
        this.setState({ tempPastSearchs: tempData });
        // this.handleTickerOptions(e.target.value);
    }
    // handleTickerOptions(val) {
    //     this.tempSearchTickerSuggestions = [];
    //     let customIndex = 0;
    //     for (let i = 0; i < this.state.pastSearchs.length; i++) {
    //         /*check if the item starts with the same letters as the text field value:*/
    //         if (this.tempSearchTickerSuggestions.length < 6) {
    //             if (this.state.pastSearchs[i].symbol.substr(0, val.length).toUpperCase() == val.toUpperCase() ||
    //                 (this.state.pastSearchs[i].name && (this.state.pastSearchs[i].name.toUpperCase().indexOf(val.toUpperCase())) > -1)) {
    //                 this.tempSearchTickerSuggestions.push({
    //                     index: customIndex,
    //                     symbol: this.state.pastSearchs[i].symbol.toUpperCase(),
    //                     name: this.state.pastSearchs[i].name
    //                 });
    //                 customIndex++;
    //             }
    //         } else {
    //             i = this.state.pastSearchs.length;
    //         }
    //     }
    //     this.setState({ searchTickerSuggestions: this.tempSearchTickerSuggestions });
    // }
    // handleKeyPress(e) {
    //     // console.log(e.key);
    //     if (e.keyCode == 13) {
    //         this.userSelectedSymbolSuggestion(this.state.searchSuggestionSelectedIndex);
    //     }
    //     if (this.state.searchTickerSuggestions.length > 0) {
    //         if (e.keyCode == 38) {
    //             this.setState(prevState => {
    //                 if (prevState.searchSuggestionSelectedIndex == 0) {
    //                     return ({ searchSuggestionSelectedIndex: this.state.searchTickerSuggestions.length - 1 })
    //                 } else {
    //                     return ({ searchSuggestionSelectedIndex: prevState.searchSuggestionSelectedIndex - 1 })
    //                 }
    //             });
    //         }
    //         if (e.keyCode == 40) {
    //             this.setState(prevState => {
    //                 if (prevState.searchSuggestionSelectedIndex == this.state.searchTickerSuggestions.length - 1) {
    //                     return ({ searchSuggestionSelectedIndex: 0 })
    //                 } else {
    //                     return ({ searchSuggestionSelectedIndex: prevState.searchSuggestionSelectedIndex + 1 })
    //                 }
    //             });
    //         }
    //     }
    // }
    // userSelectedSymbol(index) {
    //     const valueFromSuggestion = this.state.searchTickerSuggestions[index].symbol;
    //     this.setState({ tickerInput: valueFromSuggestion, loadingStockData: true });
    //     this.handleTickerOptions(valueFromSuggestion);
    //     this.finishSelectingTickerSymbol();
    //     this.calculate();
    // }
    // userClickedSymbolSuggestion(index) {
    //     this.userSelectedSymbol(index);
    // }
    // userSelectedSymbolSuggestion(index) {
    //     this.userSelectedSymbol(index);
    // }
    render() {
        return (
            <React.Fragment>
                <div className={styles.container}>
                    <div className={styles.selections}>
                        <Button color="primary" size="md" style={{ whiteSpace: 'normal' }}
                            onClick={() => {
                                this.clearHistory()
                            }}>
                            Clear History
                    </Button>
                        <Input
                            autoFocus
                            spellCheck="false"
                            // onKeyDown={(event) => this.handleKeyPress(event)}
                            className={[styles.inputTicker, styles.uppercase].join(' ')}
                            value={this.state.tickerInput}
                            placeholder='Filter By Ticker'
                            onChange={(e) => this.onTickerInputChange(e)}
                            type="text" />
                        {/* {(this.state.searchTickerSuggestions.length > 0 && this.state.finishSelectingTickerSymbol == false && this.state.inputTickerfocused == true) &&
                            <div className={styles.searchSuggestionContainer}>
                                {this.state.searchTickerSuggestions.map((item) =>
                                    // custom class depending if it matches index
                                    <div key={item.index} onClick={() => this.userClickedSymbolSuggestion(item.index)}
                                        className={[styles.autoCompleteText, `${this.state.searchSuggestionSelectedIndex == item.index ?
                                            styles.autoCompleteTextSelected : styles.autoCompleteText}`].join(' ')}>
                                        {item.symbol} - {item.name}
                                    </div>
                                )}
                            </div>
                        } */}
                    </div>
                    <div className={styles.mainContainer}>
                        {this.state.pastSearchs.length > 0 ?
                            <div>
                                {/* {this.state.tempPastSearchs.map((item, index) =>
                                    <div key={index} className={styles.eachItemInCycleContainer}>
                                        <div className={styles.eachItemInCycle}>
                                            <h3>{item.stockName.toUpperCase()}</h3><h4> - {new Date(item.time).toString()}</h4>
                                        </div>
                                    </div>
                                )} */}
                                <div className={styles.tableContainer}>
                                    <BootstrapTable striped hover bordered keyField='time' data={this.state.tempPastSearchs} columns={columns} />
                                </div>
                            </div>
                            :
                            <div>
                                <h4>You have no past searches.</h4>
                            </div>
                        }
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        pastStocks: state.history.pastStocks
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        clearHistory: () => dispatch({ type: HISTORY_CLEAR })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(History);
