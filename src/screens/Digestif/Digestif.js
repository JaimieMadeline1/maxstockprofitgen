import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import styles from './Digestif.styl';
import colors from '../../colors/colors.styl';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

class Digestif extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <div className={["container", styles.container].join(' ')}>
                    <div className="row">
                        <div className="col-lg-3"></div>
                        <div className="col-lg-6">
                            <h2 className="textMiddle">Digestif</h2>
                        </div>
                        <div className="col-lg-3"></div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Digestif;
