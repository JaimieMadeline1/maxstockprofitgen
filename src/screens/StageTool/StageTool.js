import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import styles from './StageTool.styl';
import colors from '../../colors/colors.styl';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

import { TypeChooser } from "react-stockcharts/lib/helper";
import Chart from '../../charts/Chart';
import { DateRangePicker, SingleDatePicker, DayPickerRangeController } from 'react-dates';
import 'react-dates/initialize';
import { InputNumber, Select } from 'antd';
// import { gql, withApollo } from 'react-apollo';
// import { getData } from "../../charts/utils"
import { ClipLoader } from 'react-spinners';

import axios from 'axios';

const tickersList = require('../../data/tickerSearch');
const alphaVantage = require('../../constants/alphaVantage');

import { connect } from 'react-redux';
import {
    HISTORY_ADD_SEARCH
} from '../../constants/actionTypes';

import SelectOptions from './SelectOptions';

// autocomplete(document.getElementById("myInput"), countries);

class StageTool extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tickerList: [], disabledButtons: false, tickerInput: '', searchTickerSuggestions: [],
            searchSuggestionSelectedIndex: -1, inputTickerfocused: false, finishSelectingTickerSymbol: false,
            chartData: [], stockPrice: '', focusedInput: null, startDate: null, endDate: null, inputIntervalNumber: 1,
            typeOfInterval: '', loadingStockData: false
        };
        this.tempSearchTickerSuggestions = [];
        this.isPreventBlur = false;
        // this.userClickedSymbolSuggestionFunc = this.userClickedSymbolSuggestion.bind(this);
    }
    calculate(valueFromSuggestion) {
        // this.setState({ disabledButtons: true });
        this.getTickerPrice(valueFromSuggestion);
    }
    parseAlphaVantageData(dataItems, stockName) {
        if (dataItems != null && Object.keys(dataItems).length > 0) {
            let dataTemp = [];
            Object.keys(dataItems).forEach((item) => {
                // console.log(item);
                // console.log(new Date(item).getTime());
                if (item) {
                    dataTemp.push({
                        date: new Date(item),
                        open: dataItems[item]['1. open'],
                        high: dataItems[item]['2. high'],
                        low: dataItems[item]['3. low'],
                        close: dataItems[item]['4. close'],
                        volume: dataItems[item]['5. volume'],
                    });
                }
            });
            let close = dataTemp[0].close;
            dataTemp.reverse();
            // console.log(dataTemp);
            // console.log(close);
            this.setState({ chartData: dataTemp, stockPrice: close });
            // add stock name to history
            let currDate = new Date();
            // console.log( { stockName: stockName, time: currDate.getTime()} );
            this.props.historyAddSearch({ stockName: stockName, time: currDate.getTime() });
            // load off spinner
            this.setState({ loadingStockData: false });
        }
    }
    getTickerPrice(stockName) {
        // https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=MSFT&interval=15min&outputsize=full&apikey=demo
        axios.get(`https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=${stockName}&interval=30min&outputsize=full&apikey=${alphaVantage.USER_KEY}`)
            .then((response) => {
                // console.log(response);
                this.parseAlphaVantageData(response.data["Time Series (30min)"], stockName);
            })
            .catch((error) => {
                // console.log(error);
            });
    }
    onTickerInputChange(e) {
        // console.log(e.target.value);
        if (e.target.value.length >= 1) {
            this.setState({ tickerInput: e.target.value, finishSelectingTickerSymbol: false, searchSuggestionSelectedIndex: 0 });
            this.handleTickerOptions(e.target.value);
        } else {
            this.setState({ tickerInput: e.target.value, finishSelectingTickerSymbol: true, searchSuggestionSelectedIndex: -1 });
        }
    }
    handleTickerOptions(val) {
        this.tempSearchTickerSuggestions = [{
            index: 0,
            symbol: val.toUpperCase(),
            name: ''
        }];
        this.tempSearchTickerSuggestionsNotFiltered = [];
        let customIndex = 1;
        for (let i = 0; i < tickersList.tickerSearch.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            // if (this.tempSearchTickerSuggestions.length < 6) {
                if (tickersList.tickerSearch[i].symbol.substr(0, val.length).toUpperCase() == val.toUpperCase() ||
                    (tickersList.tickerSearch[i].name && (tickersList.tickerSearch[i].name.toUpperCase().indexOf(val.toUpperCase())) > -1)) {
                    let matchPercentage = 0;
                    const tickerImportance = 0.5;
                    const nameImportance = 0.5;

                    // tickersList.tickerSearch[i].symbol.length / val.length * 0.50 + tickersList.tickerSearch[i].symbol.length / val.length * 0.5
                    if (tickersList.tickerSearch[i].symbol.substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                        let temp = val.length / tickersList.tickerSearch[i].symbol.length * tickerImportance;
                        matchPercentage += temp;
                    }
                    if (tickersList.tickerSearch[i].name && (tickersList.tickerSearch[i].name.toUpperCase().indexOf(val.toUpperCase())) > -1) {
                        let temp = val.length / tickersList.tickerSearch[i].name.length * nameImportance;
                        matchPercentage += temp;
                    }

                    this.tempSearchTickerSuggestionsNotFiltered.push({
                        // index: customIndex,
                        symbol: tickersList.tickerSearch[i].symbol.toUpperCase(),
                        name: tickersList.tickerSearch[i].name,
                        matchPercentage: matchPercentage
                    });
                    // customIndex++;
                }
            // } else {
            //     i = tickersList.tickerSearch.length;
            // }
        }
        // Sort by percentage of match
        // this.tempSearchTickerSuggestionsNotFiltered.sort((a, b) => {
        const compare = (a, b) => {
            if (a.matchPercentage < b.matchPercentage)
                return 1;
            if (a.matchPercentage > b.matchPercentage)
                return -1;
            return 0;
        }
        this.tempSearchTickerSuggestionsNotFiltered.sort(compare);
        // console.log(this.tempSearchTickerSuggestionsNotFiltered);

        // Take top 6.
        for (let i = 0; i < this.tempSearchTickerSuggestionsNotFiltered.length; i++) {
            if (this.tempSearchTickerSuggestions.length < 6) {
                this.tempSearchTickerSuggestions.push({
                    index: customIndex,
                    symbol: this.tempSearchTickerSuggestionsNotFiltered[i].symbol.toUpperCase(),
                    name: this.tempSearchTickerSuggestionsNotFiltered[i].name
                });
                customIndex++;
            } else {
                i = this.tempSearchTickerSuggestionsNotFiltered.length;
            }
        }

        // Update list
        this.setState({ searchTickerSuggestions: this.tempSearchTickerSuggestions });
    }
    finishSelectingTickerSymbol() {
        this.setState({ finishSelectingTickerSymbol: true });
    }
    userSelectedSymbol(index) {
        const valueFromSuggestion = this.state.searchTickerSuggestions[index].symbol;
        this.setState({ tickerInput: valueFromSuggestion, loadingStockData: true });
        // this.handleTickerOptions(valueFromSuggestion);
        this.finishSelectingTickerSymbol();
        this.calculate(valueFromSuggestion);
    }
    userClickedSymbolSuggestion(index) {
        this.userSelectedSymbol(index);
    }
    userSelectedSymbolSuggestion(index) {
        this.userSelectedSymbol(index);
    }
    onBlur() {
        if (!this.isPreventBlur) {
            this.setState({ inputTickerfocused: false })
        }
    }
    onFocus() {
        this.setState({ inputTickerfocused: true })
    }
    handleKeyPress(e) {
        if (this.state.searchSuggestionSelectedIndex != -1) {
            // console.log(e.key);
            if (e.keyCode == 13) {
                this.userSelectedSymbolSuggestion(this.state.searchSuggestionSelectedIndex);
            }
            if (this.state.searchTickerSuggestions.length > 0) {
                if (e.keyCode == 38) {
                    this.setState(prevState => {
                        if (prevState.searchSuggestionSelectedIndex == 0) {
                            return ({ searchSuggestionSelectedIndex: this.state.searchTickerSuggestions.length - 1 })
                        } else {
                            return ({ searchSuggestionSelectedIndex: prevState.searchSuggestionSelectedIndex - 1 })
                        }
                    });
                }
                if (e.keyCode == 40) {
                    this.setState(prevState => {
                        if (prevState.searchSuggestionSelectedIndex == this.state.searchTickerSuggestions.length - 1) {
                            return ({ searchSuggestionSelectedIndex: 0 })
                        } else {
                            return ({ searchSuggestionSelectedIndex: prevState.searchSuggestionSelectedIndex + 1 })
                        }
                    });
                }
            }
        }
    }
    inputNumberChange(value) {
        // console.log(value);
        this.setState({ inputIntervalNumber: value });
    }
    handleTypeOfIntevalChange(value) {
        this.setState({ typeOfInterval: value });
    }
    preventBlur() {
        this.isPreventBlur = true;
    }
    allowBlur() {
        this.isPreventBlur = false;
    }
    render() {
        return (
            <React.Fragment>
                <div className="sweetLoading">
                    <ClipLoader
                        color={colors.spinner}
                        size={32}
                        loading={this.state.loadingStockData}
                    />
                </div>
                <div className={["container"].join(' ')}>
                    <div className={["row", styles.tickerInputContainer].join(' ')}>
                        {/* {this.state.chartData.length > 0 ? */}
                        <React.Fragment>
                            {this.state.chartData.length > 0 &&
                                <div className="col-lg-12">
                                    {this.state.stockPrice != '' && <Label>Lastest Stock Price: {this.state.stockPrice}</Label>}
                                    <div>
                                        <TypeChooser>
                                            {type => <Chart type={type} data={this.state.chartData} tickerName={this.state.tickerInput} />}
                                        </TypeChooser>
                                    </div>
                                </div>
                            }
                            <React.Fragment>
                                <div className="col-lg-3"></div>
                                <div className={["col-lg-6", styles.mainInput].join(' ')}>
                                    {/* <div className="textMiddle">Enter Ticker</div> */}
                                    {/* <Button
                                        disabled={this.state.disabledButtons}
                                        className={styles.mainButton} outline color="primary" style={{ whiteSpace: 'normal' }}
                                        onClick={() => { this.calculate() }}>
                                        {this.state.tickerList.length > 0 ? 'Add' : 'Calculate'}
                                    </Button> */}
                                    <Input
                                        autoFocus
                                        spellCheck="false"
                                        onFocus={() => this.onFocus()}
                                        onBlur={() => this.onBlur()}
                                        onKeyDown={(event) => this.handleKeyPress(event)}
                                        className={[styles.inputTicker, styles.uppercase].join(' ')}
                                        value={this.state.tickerInput}
                                        placeholder='Enter Stock Ticker'
                                        onChange={(e) => this.onTickerInputChange(e)}
                                        type="text" />
                                    <div style={(this.state.searchTickerSuggestions.length > 0 &&
                                        this.state.finishSelectingTickerSymbol == false && this.state.inputTickerfocused == true) ?
                                        {} : { display: 'none' }}>
                                        {/* { && */}
                                        <div className={styles.searchSuggestionContainer}>
                                            {this.state.searchTickerSuggestions.map((item) =>
                                                // custom class depending if it matches index
                                                <SelectOptions key={item.index} data={item}
                                                    searchSuggestionSelectedIndex={this.state.searchSuggestionSelectedIndex}
                                                    preventBlur={() => this.preventBlur()} allowBlur={() => this.allowBlur()}
                                                    onClickEvent={(index) => this.userClickedSymbolSuggestion(index)}>
                                                </SelectOptions>
                                                // <div key={item.index} onClick={this.userClickedSymbolSuggestion.bind(this, item.index)}
                                                //     className={[styles.autoCompleteText, `${this.state.searchSuggestionSelectedIndex == item.index ?
                                                //         styles.autoCompleteTextSelected : styles.autoCompleteText}`].join(' ')}>
                                                //     {item.index == 0 ?
                                                //         <div>{item.symbol}</div>
                                                //         :
                                                //         <div>{item.symbol} - {item.name}</div>
                                                //     }
                                                // </div>
                                            )}
                                        </div>
                                        {/* } */}
                                    </div>
                                </div>
                                <div className="col-lg-3"></div>
                            </React.Fragment>
                        </React.Fragment>
                        {/* } */}
                    </div>
                    <div className="row">
                        <div className="col-lg-5">
                            {/* <div className="textMiddle">Enter Start and End Date</div> */}
                            <DateRangePicker
                                startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                                endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                                onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })} // PropTypes.func.isRequired,
                                focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                            />
                        </div>
                        <div className="col-lg-2"></div>
                        <div className="col-lg-5">
                            {/* <div className="textMiddle">Enter Inteval Period</div> */}
                            <div className={styles.intervalType}>
                                <div className={styles.intervalTypeEach}><InputNumber min={1} max={100} defaultValue={this.state.inputIntervalNumber} onChange={this.inputNumberChange.bind(this)} /></div>
                                <div className={[styles.intervalTypeEach, styles.intervalTypeText].join(' ')}>{this.state.inputIntervalNumber > 1 ? 'Trades' : 'Trade'} Per</div>
                                <div className={styles.intervalTypeEach}>
                                    <Select defaultValue="Day" style={{ width: 120 }} onChange={this.handleTypeOfIntevalChange.bind(this)}>
                                        <Select.Option value="Day">Day</Select.Option>
                                        <Select.Option value="Week">Week</Select.Option>
                                        <Select.Option value="Month">Month</Select.Option>
                                        <Select.Option value="Quarter">Quarter</Select.Option>
                                    </Select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        historyAddSearch: (searchObj) => dispatch({ type: HISTORY_ADD_SEARCH, searchObj })
    }
}

export default connect(null, mapDispatchToProps)(StageTool);
