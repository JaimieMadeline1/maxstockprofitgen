import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import colors from '../colors/colors.styl';
import FaFacebookSquare from 'react-icons/lib/fa/facebook-square';
import FaTwitterSquare from 'react-icons/lib/fa/twitter-square';

class Footer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <React.Fragment>
                <div className="footer">
                    {/* <div> Share on Media</div> */}
                    <FaFacebookSquare className="iconContainer" />
                    <FaTwitterSquare className="iconContainer" />
                </div>
            </React.Fragment>
        );
    }
}

export default Footer;
