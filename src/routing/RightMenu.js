import React, { Component } from 'react';
import ReactDOM from 'react-dom';

// Routing
import { Switch, Route } from "react-router-dom";
import { Router, IndexRedirect, browserHistory } from 'react-router';

// Import Screens(high level components)
import NotFound from '../screens/NotFound';
import Account from '../screens/Account/Account';
import LogInAndRegister from '../screens/LogInAndRegister/LogInAndRegister';
import history from '../screens/history/history';
import HomePage from '../screens/HomePage/HomePage';
import Contact from '../screens/Contact/Contact';

class RightMenu extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <Switch>
                    <Route exact path="/" component={HomePage} />
                    {/* <Route path={'/account'} component={Account} /> */}
                    {/* <Route path={'/login'} component={LogInAndRegister} /> */}
                    <Route path={'/history'} component={history} />
                    <Route path={'/contact'} component={Contact} />
                    <Route component={NotFound} />
                </Switch>
            </React.Fragment>
        );
    }
}

export default RightMenu;
