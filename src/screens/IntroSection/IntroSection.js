import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import styles from './IntroSection.styl';
import colors from '../../colors/colors.styl';

import Slider from "react-slick";

const infoCycle = require('../../data/infoCycle');

// Design like the below two homepage
// https://tipe.io/
// https://graphql.org/
class IntroSection extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <div className={styles.container}>
                    {/* <h2 className="textMiddle">How it Works</h2> */}
                    <div className={["container"].join(' ')}>
                        <div className={["row"].join(' ')}>
                            <div className={[styles.explainationBoxes, 'col-lg-4'].join(' ')}>
                                <div className={[styles.explainationBoxesInside].join(' ')}>
                                    Enter Stock Ticker.
                                </div>
                            </div>
                            <div className={[styles.explainationBoxes, 'col-lg-4'].join(' ')}>
                                <div className={[styles.explainationBoxesInside].join(' ')}>
                                    Get Performance Review of a stock.
                                </div>
                            </div>
                            <div className={[styles.explainationBoxes, 'col-lg-4'].join(' ')}>
                                <div className={[styles.explainationBoxesInside].join(' ')}>
                                    Share using the provided link the output data.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default IntroSection;
